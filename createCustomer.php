<?php
    include_once ('nonLoginHeader.php');
?>

<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Login</div>

					<div class="card-body">
						<form method="POST" action="addCustomer.php">
							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">Username</label>

								<div class="col-md-6">
									<input id="email" type="text"  name="username" required autofocus>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

								<div class="col-md-6">
									<input id="password" type="password" name="password" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Full Name</label>

								<div class="col-md-6">
									<input id="password" type="text" name="nama_penumpang" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Address</label>

								<div class="col-md-6">
									<input id="password" type="text" name="alamat_penumpang" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Birthday</label>

								<div class="col-md-6">
									<input id="password" type="date" name="tanggal_lahir" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Gender</label>

								<div class="col-md-6">
									<input id="password" type="text" name="jenis_kelamin" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Telephone</label>

								<div class="col-md-6">
									<input id="password" type="text" name="telefone" required>
								</div>
							</div>

							<div class="form-group row mb-0">
								<div class="col-md-8 offset-md-4">
									<button type="submit" class="btn btn-primary">
										Register
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php
    include_once ('nonLoginFooter.php');
?>