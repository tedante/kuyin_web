<?php

include_once ('core.php');

include_once ('adminHeader.php');

$kode = substr(md5(microtime()),rand(0,26),10);

?>



<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Add Transportation</div>

					<div class="card-body">
						<form method="POST" action="addTransportation.php">
							<input id="email" type="hidden"  name="kode" value="<?php echo $kode;?>" autofocus>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Chair</label>

								<div class="col-md-6">
									<input id="password" type="number" name="total_kursi" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Keterangan</label>

								<div class="col-md-6">
									<input id="password" type="text" name="keterangan" required>
								</div>
							</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Type Transportation</label>

								<div class="col-md-6">
									<Select name="id_type_transportasi">
										<?php
										$query = "SELECT * FROM type_transportasi";
										$result = mysqli_query($con, $query); 
										if (mysqli_num_rows($result) > 0) {
											while($row = mysqli_fetch_assoc($result)) {
										?>
										<option value="<?php echo $row['id_type_transportasi']; ?>"><?php echo $row['nama_type']; ?></option>
										<?php
											}
										}
										?>
									</Select>
								</div>
							</div>

							<div class="form-group row mb-0">
								<div class="col-md-8 offset-md-4">
									<button type="submit" class="btn btn-primary">
										Add
									</button>
								</div>
							</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('adminFooter.php');

?>