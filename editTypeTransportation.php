<?php

include_once ('core.php');

include_once ('adminHeader.php');

$id = $_POST['id_petugas'];

$query = "SELECT * FROM petugas WHERE id_petugas = '$id'";

$result = mysqli_query($con, $query);

$row = mysqli_fetch_assoc($result);
?>

<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Edit Officer</div>

					<div class="card-body">
						<form method="POST" action="pEditOfficer.php">

                            <input type="hidden" value="<?php echo $row['id_petugas']; ?>" name="id_petugas">
							<div class="form-group row">
								<label for="email" class="col-md-4 col-form-label text-md-right">Username</label>

								<div class="col-md-6">
									<input id="email" type="text"  name="username" value="<?php echo $row['username']; ?>" required autofocus>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Full Name</label>

								<div class="col-md-6">
									<input id="password" type="password" name="password" required>
								</div>
							</div>

							<div class="form-group row">
								<label for="password" class="col-md-4 col-form-label text-md-right">Full Name</label>

								<div class="col-md-6">
									<input id="password" type="text" name="nama_petugas" value="<?php echo $row['nama_petugas']; ?>" required>
								</div>
							</div>

							<div class="form-group row mb-0">
								<div class="col-md-8 offset-md-4">
									<button type="submit" class="btn btn-primary">
										Edit
									</button>
								</div>
							</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('adminFooter.php');

?>