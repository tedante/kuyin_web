<?php

include_once ('core.php');

include_once ('adminHeader.php');

?>

<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">Officer</div>

					<div class="card-body">
						<a href="createOfficer.php" class="btn btn-primary">Add Data</a>

                        <br>
                        <br>

                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Username</th>
                                <th scope="col">Name</th>
                                <th scope="col">Level</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                            <?php
                            $query = "SELECT * FROM petugas";
                            $result = mysqli_query($con, $query);
                            if (mysqli_num_rows($result) > 0) {
                                while($row = mysqli_fetch_assoc($result)) {
                                    ?>
                                        <tr>
                                        <th scope="row"><?php echo $row['id_petugas']; ?></th>
                                        <td><?php echo $row['username']; ?></td>
                                        <td><?php echo $row['nama_petugas']; ?></td>
                                        <td><?php echo $row['id_level']; ?></td>
                                        <td>
                                            <form action="editOfficer.php" method="POST">
                                                <input type="hidden" name="id_petugas" value="<?php echo $row['id_petugas']; ?>">
                                                <input type="submit" value="Edit" class="btn btn-success">
                                            </form>
                                            <form action="deleteOfficer.php" method="POST">
                                                <input type="hidden" name="id_petugas" value="<?php echo $row['id_petugas']; ?>">
                                                <input type="submit" value="Delete" class="btn btn-danger">
                                            </form>
                                        </td>
                                        </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('adminFooter.php');

?>