<?php

include_once ('core.php');

include_once ('customerHeader.php');

?>

<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">Order</div>

					<div class="card-body">
						<a href="createOrder.php" class="btn btn-primary">Add Data</a>

                        <br>
                        <br>

                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Code</th>
                                <th scope="col">Date Order</th>
                                <th scope="col">Place Order</th>
                                <th scope="col">Chair Code</th>
                                <th scope="col">Rute</th>
                                <th scope="col">Destination</th>
                                <th scope="col">Check In</th>
                                <th scope="col">Total Price</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                            <?php
                            $query = "SELECT * FROM pemesanan WHERE id_pelanggan = '".$_SESSION['id']."'";
                            $result = mysqli_query($con, $query);
                            if (mysqli_num_rows($result) > 0) {
                                while($row = mysqli_fetch_assoc($result)) {
                                    ?>
                                        <tr>
                                        <th scope="row"><?php echo $row['id_pemesanan']; ?></th>
                                        <td><?php echo $row['kode_pemesanan']; ?></td>
                                        <td><?php echo $row['tanggal_pemesanan']; ?></td>
                                        <td><?php echo $row['tempat_pemesanan']; ?></td>
                                        <td><?php echo $row['kode_kursi']; ?></td>
                                        <td><?php echo $row['id_rute']; ?></td>
                                        <td><?php echo $row['tujuan']; ?></td>
                                        <td><?php echo $row['jam_cekin']; ?></td>
                                        <td><?php echo $row['total_bayar']; ?></td>
                                        <td><?php echo $row['status']; ?></td>
                                        <td>
                                            <?php
                                            if($row['status'] == 'Belum Bayar') {
                                            ?>
                                            <form action="payOrder.php" method="POST">
                                                <input type="hidden" name="id_pemesanan" value="<?php echo $row['id_pemesanan']; ?>">
                                                <input type="submit" value="Pay" class="btn btn-success">
                                            </form>
                                            <?php
                                            } else {
                                            ?>
                                            <input type="submit" value="Pay" class="btn btn-primary" disable>
                                            <?php
                                            }
                                            ?>
                                        </td>
                                        </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                            </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<?php

include_once ('customerFooter.php');

?>