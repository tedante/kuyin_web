<?php

include_once ('core.php');

include_once ('customerHeader.php');

?>

<main class="py-4">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header">Welcome Customer</div>

					<div class="card-body">
						Welcome <?php  echo $_SESSION['username'] ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php

include_once ('customerFooter.php');

?>