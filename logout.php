<?php
session_start();
session_destroy();
unset($_SESSION['login']);
unset($_SESSION['username']);
unset($_SESSION['nama_petugas']);
unset($_SESSION['status']);
header("Location: index.php");
?>